var pipe;

var crossdomain = function(){
	
	if (window.postMessage){
		
		var mainreceive = function(message){
			// console.log('Parent - incoming mesage: ' + message.data)
			var method = message.data.split(':|:');
			Login[method[0]](method[1]);
			};

		window.addEventListener 
			? window.addEventListener('message', mainreceive, false)
			: window.attachEvent('onmessage', mainreceive); // see http://ieblog.members.winisp.net/misc/TonyRoss_WritingCrossBrowserCode.html#dontUnrelated

		return function(frame, func, args, domain){
			// console.log('Parent - Outgoing mesage: ' + func+':|:'+args, domain||'*')
			try { frame.contentWindow.postMessage(func+':|:'+args, domain||'*'); } 
			catch(e){ if (console) console.log('error sending cross domain:', e); }
			}
		} 
	else {
		var incoming = function(){
			var hash = window.location.hash;
			if (!hash || hash == '#') return;
			var method = hash.split(':|:');
			window.location.hash = '#';
			(window[method[0].substr(1)]||function(){})(method[1]);
			}
		setInterval(incoming,1000);
		return function(frame, func, args, domain){
			frame.location = (domain||'') + '#' + func + ':|:' + args;
			}
		}
	}()

var Login = 
	{ Manager: function(methods){
		if (Browser.ie6){
			var ss = document.createStyleSheet(), basic = 'height:100%; overflow:auto';
			ss.addRule('html', basic); ss.addRule('body', basic);
			ss.addRule('.isFixed','position:absolute !important');
			ss.addRule('#unFixed', 'width:100%; position:relative;' + basic);
			new Element('div#unFixed').adopt($$('body>*')).inject(document.body, 'top');
			}
		//var popup = Login.popup = new Element('iframe',
		var popup = Login.popup = new IFrame(
			{ src: '<?php echo "$url?form=1"; ?>'
			, 'name': 'pop'
			, 'class': 'isFixed'
			, allowTransparency: true // http://msdn.microsoft.com/en-us/library/ms533072%28VS.85%29.aspx
			, styles: {
				position:'fixed', left:0, top:0, height:'100%', width:'100%', 'z-index':100, display:'none'
				}
			}).inject(window.document.body);
		Login.tween = new Fx.Tween(popup,
			{ duration  : 2500
			, onComplete: function(){ popup.setStyle('display','none'); }
			});
		methods = methods || {};
		['Login','Logout','BeforeLogout','Init'].each(function(event){
			Login['on'+event] = methods['on'+event] || function(){};
			});
		return Login.trigger.grab(Login.inBtn);
		}
	, trigger: new Element('span#logManSpan',{html:'Welcome <b>Guest</b>'})
	, open: function(){
		//window.frames[1].location = 'http://frumtherapist.com/hashtest.htm#rehash';
		//alert(window.frames[1].location);
		if (Login.member) return;
		Login.popup.setStyle('display','block').tween('opacity',1);
		}
	, close: function(){ Login.tween.start('opacity',0) }
	, login: function(member){
		member = member.split('_');
		Login.close();
		Login.member = member[0];
		Login.trigger.grab(Login.outBtn).getFirst('b').set('text',member[0]);
		Login.inBtn.dispose();
		Login.onLogin(member[0], !!(+member[1]));
		}
	, logout: function(){
		Login.onBeforeLogout(Login.member);
		Login.member = false;
		Login.trigger.set('html','Welcome <b>Guest</b>').adopt(Login.inBtn);
		crossdomain(Login.popup,'logout','logout');
		Login.onLogout(Login.member);
		}
	, inBtn: new Element('a[href=#]', 
		{ text: '[Log In]'
		, events: {
			click: function(e){
				e.stop();
				Login.open();
				}}
		})
	, outBtn: new Element('a[href=#]', 
		{ 'text': '[Log Out]'
		, events: {
			click: function(e){
				e.stop();
				Login.logout();
				}}
		})
	};
