window.addEvent('domready',function (){
	Login = new formFuncs();
	new HTML5().validateForms(
		{ sameCheck:
			[ function(val,check){ return val != $$('input[name=reset_1]')[0].get('value'); }
			, 'The fields do not match.'
		]});
});

var Login
  , crossdomain = function(){
  
		if (window.postMessage){
			
			var framereceive = function(message){
				// console.log('Frame - incoming message: ',message);
				var method = message.data.split(':|:');
				Login[method[0]](method[1]);
				}
			
			window.addEventListener 
				? window.addEventListener('message', framereceive, false)
				: window.attachEvent('onmessage', framereceive);

			return function(frame, func, args, domain){
				// console.log('Frame - Outgoing message: '+func+':|:'+args, domain||'*');
				if (frame == this) return;
				try { frame.postMessage(func+':|:'+args, domain||'*'); } 
				catch(e){ alert('There was an err sending from the iframe:',e) }
			}
		} else {
			function incoming(){
				var hash = window.location.hash;
				if (!hash || hash == '#') return;
				var method = hash.split(':|:');
				window.location.hash = '#';
				(window[method[0].substr(1)]||function(){})(method[1]);
			}
			setInterval(incoming,1000);
			return function(frame, func, args, domain){
				frame.location = (domain||'') + '#' + func + ':|:' + args;
			}
		}
	}()
, formFuncs = new Class(
	{ els: {}
	, initialize: function(options) {

		this.formSetup();

		var member = "<?php if (isset($_SESSION['member'])) echo $_SESSION['member']; ?>";
		var autologin = this.autologin = Cookie.read('autologin');
		if (member) crossdomain(parent,'login',member+'_1');
		else if (autologin){
			$('loginPopup').getElement('[name=rem]').set('checked',true);
			if ('<?php echo !$_GET['reset']; ?>') this.submit.send(autologin);
			}
		crossdomain(parent, 'onInit', member||autologin||0);
		}

	, formSetup: function(){
		var loginForm = window.document.id('loginForm')
			, loginPopup = window.document.id('loginPopup')
			, rem = loginForm.getElement('[name=rem]')
			, self = this
			, submit = this.submit = new Request(
				{ //url: '<?php echo "$url"; ?>'
				//,
				 link: 'cancel'
				, onRequest: function(){
					if (!rem.get('checked')) Cookie.dispose('autologin');
					}
				, onSuccess: function(response){
					response = JSON.decode(response);
					switch (response[0]){
						default:
							alert('There appears to be an issue.  Please contact customer support'); break;
						case 'apply':
							loginPopup.set('class','ty apply'); break;
						case 'err':
							$('err').set({'text':response[1],'class':''}).inject(
								$('loginPopup').getElement('[name='+(response[2]||'pass')+']'),'after'); break;
						case 'log': break;
						case 'reset': loginPopup.set('class','ty reset'+response[1]); break;
						case 'newpass': loginPopup.set('class','ty newpass'+response[1]); break;
						case 'member':
							if (rem.get('checked')) Cookie.write('autologin','user='+response[1]+'&pass='+response[2], {duration:30});
							crossdomain(parent,'login',response[1]+'_'+self.autologin);
							// Cookie.write('member', response[1]);	// Write username into a session cookie.
							// if (parent.Login) parent.Login.login(response[1]);					
						}
					}
				});

		loginForm.addEvent('submit:relay(form)',function(e){
			e.stop();
			submit.send(this);
			});
	
		['in','up','set'].each(function(btn){
			$$('.'+btn+'Link').addEvent('click',function(e){
				loginPopup.set('class', loginPopup.get('class').replace(/in|up|ty|set/g,'')+' '+btn);
				e.stop();
				})
			});
		
		$$('.x').addEvent('click', self.closeLogin);
	}
	, closeLogin: function(){
		crossdomain(parent,'close','close');
	}
	, logout: function(arg){
		if (arg != 'logout') return
		formFuncs.autologin = false;
		Cookie.dispose('autologin');
		this.submit.send('logout=true');
	}
	, login: function(arg){ $$('[name=rem]')[0].set('checked',true); Login.submit.send(arg) }
});
