<?php

##################### Variables #####################

$defaults = array
	( 'database' => 'accounts'
	, 'table'  => 'members'
	, 'myUser' => ''
	, 'myPass' => ''
	
	, 'site' => $_SERVER['HTTP_HOST']
	, 'from' => "{$_SERVER['HTTP_HOST']} <no-reply@{$_SERVER['HTTP_HOST']}>"
	, 'debug'=> false
	);

################ Generic Functions ################## 

function URL($get=false){
	$url = array_shift(explode('/',$_SERVER["SERVER_PROTOCOL"]));
	if (@$_SERVER["HTTPS"] == 'on') $url .= 's';
	$url = strtolower($url.'://'.$_SERVER['HTTP_HOST']);
	if (($port = $_SERVER["SERVER_PORT"]) != "80") $url .= ":$port";
	return $url .= $get ? $_SERVER['REQUEST_URI'] : $_SERVER['SCRIPT_NAME'];
}
function sanitize($vals,$html=true){
	$val = '';
	if(!$vals = (array)$vals) return false;
	foreach ($vals as &$val){
		if ($html) $val = htmlentities($val,ENT_NOQUOTES,'UTF-8',false);
		if (is_array($val))
			foreach ($val as &$v) $v = sanitize($v);
		elseif (@mysql_ping()){
			if (get_magic_quotes_gpc()) $val = stripslashes($val);
			$val = mysql_real_escape_string(trim($val));
		} elseif (!get_magic_quotes_gpc()) $val = addslashes(trim($val));
	}
	return count($vals) > 1 ? $vals : $val;
}
function filter($array, $accept, $global = false, $sanitize = false){
	$accepted = array();
	if (!is_array($accept)) $accept = explode(',',$accept);
	foreach($accept as $v){
		if ($array[$v]){
			if ($global) global $$v;
			$$v = $accepted[$v] = $sanitize ? sanitize($array[$v]) : $array[$v];
		}
	}
	return $accepted;
}

################## Database Class ###################

class dbFuncs {	
	function __construct($vars) {
		foreach($vars as $k=>$v) $this->$k = $v;
		$this->mysql = $this->myUser && $this->myPass;
		$this->newTable = 'CREATE TABLE IF NOT EXISTS `{$this->table}` 
			( `id` INT(10) NOT NULL AUTO_INCREMENT
			, `user` VARCHAR(150) NOT NULL
			, `email` VARCHAR(150) NOT NULL
			, `pass` TINYTEXT NOT NULL
			, `verified` VARCHAR(9) NULL DEFAULT NULL
			, `visit` DATETIME NULL DEFAULT NULL
			, `created` DATETIME NULL DEFAULT NULL
			, PRIMARY KEY (`id`)
			, UNIQUE INDEX `email` (`email`)
			, UNIQUE INDEX `user` (`user`)
			)';
	}
	function dbconnect(){
		$link = self::myConnect();
		if (is_array($link)) self::createDB();
		if (!self::query("SHOW TABLES LIKE '{$this->table}';",3)) self::query($this->newTable);
		return $link;
	}
	function myConnect(){
		$link = mysql_connect("localhost", $this->myUser, $this->myPass);
		if (!$link) $link = self::errHandler
			(mysql_error(), mysql_errno(), "mysql_connect('localhost', {$this->myUser}, {$this->myPass})");
		if (!mysql_select_db($this->database)) $link = self::errHandler
			(mysql_error(), mysql_errno(), "mysql_select_db({$this->database})");
		return $link;
	}
	function query($sql, $flat = 0, $conn='', $array=1){
		$query = mysql_query($sql);  //var_dump($sql);
		if (!$query) return self::errHandler(mysql_error(),mysql_errno(),$sql);
		if (!$array || is_bool($query)) return $query;
		
		$results = array();
		while ($result = mysql_fetch_assoc($query)) $results[] = $result;
		if ($results && ($flat == 1 || ($flat && count($results) == 1))) $results = array_shift($results);
		if ($flat == 3) $results = array_shift($results);
		return $results;
	}
	function errHandler($err, $errno, $query, $mail=false){
		$message  = '<b>MySQL error:</b><br>' . $err . '<br><br>'
				   .'<b>Whole query:</b><br>' . $query . '<br><br>';
		if ($mail) echo 'mail not set up correctly';
		if ($this->debug) die ($message);
		return array('err',$errno,$err);
	}
	function createDB(){
		$newDb = $this->mysql ? "CREATE DATABASE IF NOT EXISTS {$this->database};" : '';
		$link = self::query($newDb);
		self::query($this->newTable);
		return $link;
	}
}

include 'defaults.php';
$db = new dbFuncs($defaults);
$url = URL();

session_start();

#if(!($_GET || $_POST) || isset($_GET['js'])){
if (empty($_REQUEST) || isset($_GET['js'])){
	Header("content-type: application/x-javascript");
	####################################################
	#######   JS File included in main file   ##########
	####################################################
	include 'parent.js';
 	exit; 
 	}
 
elseif (isset($_POST['logout'])){
	
	unset($_SESSION['member']);
	if (isset($_COOKIE[session_name()])) setcookie(session_name(), '', time()-42000, '/');
	session_destroy();
	exit ("['log','out']");
	//foreach (array_keys($_COOKIE) as &$C) setcookie($C,'',time()-1);
	}

####################################################
############     Password Reset     ################
####################################################
	
# Pass Reset 1: Receives user/email from form requesting reset. Sends confirmation mail.
elseif (isset($_POST['reset'])){

	# Send email to reset password.
	$site = $defaults['site'];
	$user = $_POST['reset']; // Manual sanitization in next line
	if (!preg_match('/^(\w+|[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})$/i', $user)) 
		exit("['err','please use a valid username or password']");

	$rand = uniqid("reset",true);
	$link = $db->dbconnect();
	$select = $db->query("SELECT email,user FROM {$db->table} WHERE (email LIKE '$user' OR user LIKE '$user')",1);
	$update = $db->query("UPDATE {$db->table} SET realpass='$rand' WHERE `user` LIKE '{$select['user']}'");

	$mail = (int)mail // array // ToDo: Clean 'to' field to prevent malicious bcc's.
		( "{$select['user']} <{$select['email']}>"
		, "Password reset request: $site"
		, " <p>A password reset request was initiated for $site.</p>
			<p>To confirm and choose a new password, please click <a href='$site/$rand'>here</a>. 
			<br>Or copy and paste the following URL into your browser's address bar:
			<br>$site/$rand</p>
			<p>If you did not initiate this request, you can safely ignore this email, no private information was shared.</p>
			<p>Thank you for using Siteroller</p>"
		, "From: {$defaults['from']}\nMIME-Version:1.0\nContent-type:text/html\r\n"
		);

	mysql_close($link);
	exit("['reset',$mail]"); // json_encode($mail)
	}

# Pass Reset 3: Receives new pass from form. Send confirmation mail.		 
elseif (isset($_POST['reset_1'])){

	if (!$user = $_SESSION['member']) exit('["err","Your reset link has expired."]');
	$link = $db->dbconnect();
	$pass = sanitize($_POST['newpass']);
	$update1 = "UPDATE {$db->table} SET pass = '".md5($pass)."', realPass = '$pass' WHERE `user` LIKE '$user'";
	$update = $db->query($update1)
		? " <p>Your password has been reset for {$defaults['site']}.</p><p>Thank you for using Siteroller.</p>"
		: " <p>We are sorry, but your password cannot be reset. Support has been notified, and you should hear from us soon.</p>";
	$mail = (int)mail // array
		( " $user <{$_SESSION['email']}>"
		, " Password reset request: {$defaults['site']} "
		, $update
		, "From: {$defaults['from']}\nMIME-Version:1.0\nContent-type:text/html\r\n"
		);

	mysql_close($link);
	exit("['newpass',$mail]"); // json_encode($mail)
	}
	
# Pass Reset 3: User has clicked link sent to his email. (Listed after #2, as GET['reset'] is always true.)
elseif (isset($_GET['reset'])){
	
	$reset = sanitize($_GET['reset']);
	$link = $db->dbconnect();
	$select = $db->query("SELECT user,email FROM {$db->table} WHERE realPass LIKE BINARY '$reset'", 1);
	$_SESSION['member'] = $select['user'];
	$_SESSION['email'] = $select['email'];
	
	mysql_close($link);
	$select = $link = $reset = null;
	
	$tab = $_SESSION['member'] ? 'set newpass' : 'ty expired';
	}
	
	
	
		
elseif (isset($_GET['u'])){

	# Account Verification
	$link = $db->dbconnect();
	$u = $v = '';
	extract(sanitize(filter($_GET,'u,v')));
	$db->query("UPDATE {$db->table} SET verified='Y' WHERE `user`='$u' AND verified='$v';");
	$rows = mysql_affected_rows();
	$result = $db->query("SELECT (email,verified) WHERE `user`='$u'",1);
	if ($rows){
		$subject = "Welcome to $site";
		$body = "Thank you for creating an account at $site. We look forward to serving you.";
		mail("$name <$email>", $subject, $body, "From: {$defaults['from']}");
		$html = 'Thank you for verifying your account.';
		} else $result['verified'] == 'Y'
			? "Thank you for creating an account at $site"
			: 'Please contact support if you are having issues verifying your account.';
		
	mysql_close($link);
	$tab = 'ty confirm';
	}
			 
elseif($_POST) {

	$link = $db->dbconnect();
	$post = sanitize(filter($_POST,'user,email,pass'));
	$post['pass'] = md5($post['pass']);
	$user = $name = $verification = '';
	extract($post);
	
	function err($e){
		$errs = array(
			'All fields are required.'
			, 'Four characters, letters and digits only.'
			, "Four characters, letters and digits only.','user"
			, "Please use a standard email address format.','email"
			, "Username is not available.','user"
			, "This email is previously registered.','email"
			, 'Standard email or valid username.'
			, 'Username and password do not match.'
			, 'Password does not match.'
			, "Username could not be found.','user"
			, 'Your account could not be created'
			, 'There was a problem logging in'
		);
		$err = isset($errs[$e]) ? $errs[$e] : $e;
		die ("['err','$err']"); 
	}
	
	# Validate pass. Should catch empty strings as well.
	if (!preg_match('/^\w{4,}$/i', $pass)) err(1); 
	
	if ($_POST['email']){
		
		# Validate user and email.
		if (!preg_match('/^\w{4,}$/i', $user)) err(2);
		if (!preg_match('/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/is', $email)) err(3);
		
		# Generate Verification code.
		$verified = substr(mt_rand(), 0, 9);
		
		# Query Database
		$query = "INSERT INTO {$db->table} (`user`,email,pass,verified) VALUES ('$user','$email','$pass',$verified);"; //var_dump($query);
		$result = $db->query($query,1); //var_dump($result);
		
		# Handle errors gracefully 
		if (!$result) err(10);
		elseif ($result[0] == 'err'){
			if ($result[1] == 1062) {
				$num = array_pop(explode('key ',$result[2]));
				$num = ($num == "'user'" ? 4 : 5);
				err($num);
			} else err($result[1]);
		}
		
		#Send Verification mail
		$subject = "Please confirm your $site account";
		$href = "$url?u=$user&v=$verified";
		$body = "To confirm your $site account, please click <a href='$href'>here</a>. Or copy and paste the following URL into your browser's address bar.";
		$mail = @mail("$name <$email>", $subject, $body, "From: {$defaults['from']}");
		mysql_close($link);
		exit ("['apply','".$mail."']");
		
	} else {

		# Member has logged in

		$query = "SELECT id,user,cookie FROM {$db->table} 
			WHERE (`email` LIKE '$user' OR `user` LIKE '$user')
			AND  (`pass` LIKE BINARY '$pass' OR `cookie` LIKE BINARY '$pass')";
		
		if (!preg_match('/^(\w+|[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})$/i', $user)) err(6);
		$result = $db->query($query,1) or err(7); //var_dump('result',$result,$query);
		if (isset($result[0]) && $result[0] == 'err') err($result[1]);
		
		session_regenerate_id();
		$user = $_SESSION['member'] = $result['user'];
		session_write_close();

		# Update autologin password. Generate a new one each login for security.
		$cookie = uniqid(rand()) + 1001;
		$res2 = $db->query("UPDATE users SET cookie = '".md5($cookie)."' WHERE user = '$user'",1) or err(7); 
		//var_dump('result',$result, $cookie);
		
		mysql_close($link);
		exit ($result['user'] ? "['member','{$result['user']}', '$cookie']" : "['err','11']"); // returns username or err message
	}
}
	
####################################################
#######            HTML IFrame            ##########
####################################################
require 'iframe.html';
?>